from setuptools import setup
from setuptools import find_packages

setup(
  name = "astrum",
  version = "0.0.1",
  license = "MIT",
  packages= find_packages(),
  install_requires=[
    'argparse',
    'dateparser',
    'ephem',
    'lxml',
    'pylatex',
  ]
)
