astrum is a Python-based planetary position calculator.


# Bootstrapping the virtual environment

The virtual environment must have pip installed so that other things can be
installed:

> ln -s /usr/bin/python3 bin/python3
> activate
> python3 -m ensurepip

Then we must install ourselves:

> pip install -e .

# Other pip installations

The following should be provided automatically using XXX FIXME.

* pip install argparse
* pip install lxml
* pip install dateparser
