#!/usr/bin/env python3
# year-summary.py -*-python-*-
# Copyright 2017 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

# Standard imports
import datetime

# Imports that might have to be installed
try:
    import dateparser
except ImportError as e:
    print('''\
# Cannot import dateparser: {}
# Consider: apt-get install python3-dateparser'''.format(e))
    raise SystemExit

# Imports that might have to be installed
try:
    import ephem
except ImportError as e:
    print('''\
# Cannot import ephem: {}
# Consider: apt-get install python3-ephem'''.format(e))
    raise SystemExit

try:
  from pylatex import Document, Section, UnsafeCommand
  from pylatex.base_classes import Environment, CommandBase, Arguments
  from pylatex.package import Package
  from pylatex.table import Tabular
  from pylatex.utils import NoEscape
except ImportError as e:
    print('''\
# Cannot import pylatex: {}
# Consider: pip3 install pylatex'''.format(e))
    raise SystemExit

import astrum
from astrum.astrum import Astrum
from astrum.location import Location
from astrum.log import DEBUG, INFO, ERROR, FATAL
from astrum.time import Time

class Supertabular(Tabular):
  packages = [Package('supertabular')]

class YearSummary(object):
  BASE_TIME = '7:00'

  def __init__(self, year, city, latitude, longitude, extended = False):
    self.year = year
    self.city = city
    self.latitude = latitude
    self.longitude = longitude
    self.extended = extended

    self.oneday = datetime.timedelta(1)
    self.twodays = datetime.timedelta(2)

    previous_year = '%4d/12/31 23:59' % (year - 1)
    self.date = dateparser.parse('January 1, %d %s' % (year,
                                                       YearSummary.BASE_TIME))
    self.next_full_moon = ephem.localtime(ephem.next_full_moon(previous_year))
    self.next_new_moon = ephem.localtime(ephem.next_new_moon(previous_year))
    self.next_first_quarter = ephem.localtime(ephem.next_first_quarter_moon(previous_year))
    self.next_last_quarter = ephem.localtime(ephem.next_last_quarter_moon(previous_year))
    self.next_equinox = ephem.localtime(ephem.next_equinox(previous_year))
    self.next_solstice = ephem.localtime(ephem.next_solstice(previous_year))

    # Determine times zone abbreviations for year
    self.location = Location(self.city, self.latitude, self.longitude)
    self.latitude = self.location.latitude
    self.longitude = self.location.longitude
    INFO('%s %s' % (self.date, self.location.timezone))
    time = Time(self.date, default_timezone = self.location.timezone)
    self.zones = [time.timezone]
    date = self.date
    for i in range(1, 52):
      time = Time(date, default_timezone = self.location.timezone)
      if time.timezone not in self.zones:
        self.zones.append(time.timezone)
      date = date + datetime.timedelta(7 * i)
    INFO('Time zones present: %s' % str(self.zones))

    self.create_doc()

  @staticmethod
  def same_day(d0, d1):
    if not d0 or not d1:
      return False
    if d0.day != d1.day:
      return False
    if d0.month != d1.month:
      return False
    if d0.year != d1.year:
      return False
    return True

  def pretty_time(self, date):
    if self.extended:
      return '%d:%02d' % (date.hour, date.minute)
    else:
      if date.hour < 12:
        return '%d:%02da' % (date.hour, date.minute)
      hour = date.hour - 12
      if hour == 0:
        hour = 12
      return '%d:%02dp' % (hour, date.minute)

  def create_table(self, doc, heading, page):
    if self.extended:
      doc.append(NoEscape(r'%s' % heading +
                          r'\textbf{, Page %s}' % page +
                          '\n\n'))
    else:
      doc.append(NoEscape(r'\twocolumn[%s]' % heading +
                          r'\textbf{, Page %s}' % page +
                          '\n\n'))

    if self.extended:
      table = Tabular('lrllllllllllllrrrrrl')
    else:
      table = Tabular('lllllllllll')
    row = []
    row.append(NoEscape(r'\textbf{Date}'))
    row.append('')
    row.append('')
    row.append('')
#    row.append(NoEscape(r'\textbf{Asc}'))
#    row.append(NoEscape(r'\textbf{Mc}'))
    for name, sect in Astrum.PLANETS:
      row.append(NoEscape(r'\textbf{\%s}' % name))
    if self.extended:
      row.append(NoEscape(r'\textbf{\Sun{}Rise}'))
      row.append(NoEscape(r'\textbf{\Sun{}Set}'))
      row.append(NoEscape(r'\textbf{m/h}'))
      row.append(NoEscape(r'\textbf{\Moon{}Rise}'))
      row.append(NoEscape(r'\textbf{\Moon{}Set}'))
      row.append(NoEscape(r'\textbf{Holiday}'))
#      row.append(NoEscape(r'\textbf{Aspects}'))
    table.add_row(row)
    return table

  def compute_special_column(self, extra, sun, ast):
    s = extra

    s += r'\moon[scale=.7]{%f}\,' % ast.moon_fractional_age
    if self.same_day(self.date, self.next_full_moon):
      s += self.pretty_time(self.next_full_moon)
      self.next_full_moon \
        = ephem.localtime(ephem.next_full_moon(self.date + self.twodays))
    elif self.same_day(self.date, self.next_new_moon):
      s += self.pretty_time(self.next_new_moon)
      self.next_new_moon \
        = ephem.localtime(ephem.next_new_moon(self.date + self.twodays))
    elif self.same_day(self.date, self.next_first_quarter):
      s += self.pretty_time(self.next_first_quarter)
      self.next_first_quarter \
        = ephem.localtime(ephem.next_first_quarter_moon(self.date + self.twodays))
    elif self.same_day(self.date, self.next_last_quarter):
      s += self.pretty_time(self.next_last_quarter)
      self.next_last_quarter \
        = ephem.localtime(ephem.next_last_quarter_moon(self.date + self.twodays))

    self.holiday = None

    if self.same_day(self.date, self.next_solstice):
      _, _, _, sign, *_ = sun
      if sign % 2 == 0:
        sign += 1
      sign %= 12
      s += r'\%s{}' % Astrum.SIGNS[sign] + self.pretty_time(self.next_solstice)
      if sign == 3:
        self.holiday = 'Litha/Summer Solstice'
      else:
        self.holiday = 'Yule/Winter Solstice'
      self.next_solstice \
        = ephem.localtime(ephem.next_solstice(self.date + self.twodays))

    if self.same_day(self.date, self.next_equinox):
      _, _, _, sign, *_ = sun
      if sign % 2 != 0:
        sign += 1
      sign %= 12
      s += r'\%s{}' % Astrum.SIGNS[sign] + self.pretty_time(self.next_equinox)
      if sign == 0:
        self.holiday = 'Ostara/Spring Equinox'
      else:
        self.holiday = 'Mabon/Fall Equinox'
      self.next_equinox \
        = ephem.localtime(ephem.next_equinox(self.date + self.twodays))

    return NoEscape(s)

  def format_sign(self, extra, planet_name, sign_idx, offset,
                  rejoicing = False, retrograde = False):
    sign_name = Astrum.SIGNS[sign_idx]
    if planet_name == '':
      escaped_planet_name = ''
    else:
      escaped_planet_name = '\\' + planet_name

    if retrograde:
      if planet_name == 'Moon' or planet_name == 'Sun':
        FATAL('Moon and Sun cannot be retrograde')
      color = 'red'
      sign_color = 'red'
    else:
      color = 'black'
      sign_color = Astrum.SIGN_COLORS[Astrum.SIGNS[sign_idx]]

    dignities = ''
    if planet_name != '':
      for dignity_idx, abbreviation in enumerate(Astrum.DIGNITY_ABBREVIATIONS):
        if sign_name in Astrum.DIGNITIES[planet_name][dignity_idx]:
          dignities += abbreviation

    if sign_color == 'yellow':
      s = (extra
           + '{\\color{%s}{%s}{\\tiny %s}%s' % (color,
                                                escaped_planet_name,
                                                dignities,
                                                int(offset + 0.5))
           + '\\contour{black}{\\textcolor{yellow}{\\%s}}}\n' % sign_name)
    else:
      s = (extra
           + '{\\color{%s}{%s}{\\tiny %s}%s\\color{%s}{\\%s}}\n'
           % (color, escaped_planet_name, dignities,
              int(offset + 0.5), sign_color, sign_name))
    return s

  def create_doc(self):
    doc = Document(document_options = 'twocolumn' if not self.extended else '',
                   fontenc = None,
                   inputenc = None)
    doc.packages.append(Package('./magickfonts'))
    doc.packages.append(Package('xcolor', 'table'))
    doc.packages.append(Package('wasysym'))
    doc.packages.append(Package('geometry', 'margin=0.3in,nofoot'))
    doc.packages.append(Package('contour'))
    doc.packages.append(Package('./moonphases'))
    doc.packages.append(NoEscape(r'\contournumber{8}'))
    doc.packages.append(NoEscape(r'\pagestyle{empty}'))
    doc.packages.append(NoEscape(r'\parindent 0pt'))
    doc.packages.append(NoEscape(r'\parskip 0pt'))
    doc.packages.append(NoEscape(r'\def\Sun{\Sol}'))
    doc.packages.append(NoEscape(r'\def\Moon{\Luna}'))

    #doc.append(NoEscape(r'\scriptsize'))
    doc.append(NoEscape(r'\footnotesize'))
    doc.append(NoEscape(r'\renewcommand{\baselinestretch}{.8}'))
    doc.append(NoEscape(r'\setlength{\tabcolsep}{%sem}'
                        % (0.1 if not self.extended else 0.2)))
    doc.append(NoEscape(r'\setlength{\textheight}{1.15\textheight}'))
    doc.append(NoEscape(r'\renewcommand{\arraystretch}{0.7}'))

    heading = r'\raggedright\textbf{%d: %s at %s; times are local (%s/%s)}' % (
      self.year, self.location, YearSummary.BASE_TIME,
      self.zones[0], self.zones[1])

    count = 0
    table_count = 0
    page = 1
    table = self.create_table(doc, heading, page)
    cellcolor = False
    while True:
      if count % 3 == 0:
        cellcolor = not cellcolor
      if cellcolor:
        extra = r'\cellcolor{blue!25}'
      else:
        extra = ' '

      ast = Astrum(self.date, self.city, self.latitude, self.longitude)

      row = [ NoEscape(extra + self.date.strftime('%a')),
              NoEscape(extra + self.date.strftime('%e')),
              NoEscape(extra + self.date.strftime('%b') + '\n')
      ]

      row.append(self.compute_special_column(extra, ast.planets['Sun'], ast))

#      row.append(NoEscape(self.format_sign(extra, '',
#                                           ast.ascendant_sign,
#                                           ast.ascendant_offset)))
#      row.append(NoEscape(self.format_sign(extra, '',
#                                           ast.midheaven_sign,
#                                           ast.midheaven_offset)))
      for planet_name, sect in Astrum.PLANETS:
        rejoicing, retrograde, _, sign, offset, *_ = ast.planets[planet_name]
        row.append(NoEscape(self.format_sign(extra, planet_name, sign, offset,
                                             rejoicing, retrograde)))


      if self.extended:
        if self.same_day(self.date, ast.sun_rise):
          row.append(NoEscape(extra + self.pretty_time(ast.sun_rise)))
        else:
          row.append(NoEscape(extra))

        if self.same_day(self.date, ast.sun_set):
          row.append(NoEscape(extra + self.pretty_time(ast.sun_set)))
        else:
          row.append(NoEscape(extra))

        hours_per_day = ast.sun_set - ast.sun_rise
        row.append(NoEscape(extra + '%.1f' % (hours_per_day.total_seconds() /
                                              60 / 12)))

        if self.same_day(self.date, ast.moon_rise):
          row.append(NoEscape(extra + self.pretty_time(ast.moon_rise)))
        else:
          row.append(NoEscape(extra))

        if self.same_day(self.date, ast.moon_set):
          row.append(NoEscape(extra + self.pretty_time(ast.moon_set)))
        else:
          row.append(NoEscape(extra))

        if False:
          aspects = extra
          for angle, planet, aspect, difference, aspected in ast.aspects:
            if aspect in ['Semisextile', 'Inconjunct']:
              continue
            aspects += '{\\%s\\%s\\%s}\,' % (planet,
                                             aspect,
                                             aspected)
          row.append(NoEscape(aspects))

        if self.same_day(self.date, datetime.datetime(self.date.year, 10, 31)):
          row.append(NoEscape(extra + 'Samhain'))
        elif self.same_day(self.date, datetime.datetime(self.date.year, 2, 2)):
          row.append(NoEscape(extra + 'Imbolc/Candlemas'))
        elif self.same_day(self.date, datetime.datetime(self.date.year, 5, 1)):
          row.append(NoEscape(extra + 'Beltane'))
        elif self.same_day(self.date, datetime.datetime(self.date.year, 8, 1)):
          row.append(NoEscape(extra + 'Lughnasadh/Lammas'))
        elif self.holiday:
          row.append(NoEscape(extra + self.holiday))
        else:
          row.append(NoEscape(extra))

      table.add_row(row)
      self.date += self.oneday
      if self.date.day == 1 and (self.date.month == 4
                                 or self.date.month == 7
                                 or self.date.month == 10
                                 or self.date.month == 1):
        doc.append(table)
        table_count += 1
        s = ''
        t0 = r'''\begin{tabular}[t]{|ll|}
        r & rulership \\
        d & detriment \\
        e & exaltation \\
        f & fall\\
        \end{tabular}'''
#        t1 = r'''\begin{tabular}[t]{ll}
#        Asc & Ascendant (Rising Sign) \\
#        Mc & Medium Coeli (Midheaven) \\
#        \color{red}{red text} & planet is retrograde \\
#        \end{tabular}'''
        t1 = r'''\begin{tabular}[t]{ll}
        \color{red}{red text} & planet is \\
                              & retrograde \\
        \end{tabular}'''
        t2 = r'''\begin{tabular}[t]{|ll}
        \fullmoon & Full Moon \\
        \newmoon & New Moon \\
        \Aries & Spring Equinox \\
        \Cancer & Summer Solstice \\
        \Libra & Fall Equinox \\
        \Capricorn & Winter Solstice \\
        \end{tabular}'''
        t3 = r'''\begin{tabular}[t]{|ll}
        \Sol & Sol \\
        \Luna & Luna \\
        \Mercury & Mercury \\
        \Venus & Venus \\
        \Mars & Mars
        \end{tabular}'''
        t3b = r'''\begin{tabular}[t]{|ll}
        \Jupiter & Jupiter \\
        \Saturn & Saturn \\
        \Uranus & Uranus \\
        \Neptune & Neptuns \\
        \Pluto & Pluto
        \end{tabular}'''
        if False:
          t4 = r'''\begin{tabular}[t]{|ll|}
          \color{red}{\Aries} & \color{red}{Aries} \\
          \color{black}{\Taurus} & \color{black}{Taurus} \\
          \contour{black}{\textcolor{yellow}{\Gemini}} &
          \contour{black}{\textcolor{yellow}{Gemini}} \\
          \color{blue}{\Cancer} & \color{blue}{Cancer} \\
          \color{red}{\Leo} & \color{red}{Leo} \\
          \color{black}{\Virgo} & \color{black}{Virgo} \\
          \contour{black}{\textcolor{yellow}{\Libra}} &
          \contour{black}{\textcolor{yellow}{Libra}} \\
          \color{blue}{\Scorpio} & \color{blue}{Scorpio} \\
          \color{red}{\Sagittarius} & \color{red}{Sagittarius} \\
          \color{black}{\Capricorn} & \color{black}{Capricorn} \\
          \contour{black}{\textcolor{yellow}{\Aquarius}} &
          \contour{black}{\textcolor{yellow}{Aquarius}} \\
          \color{blue}{\Pisces} & \color{blue}{Pisces} \\
          \end{tabular}'''
          #        t5 = r'''\begin{tabular}[t]{lll|}
          #        \Conjunction & conjunction & 0${}^\circ\pm 7$\\
            #        \Semisextile & semisextile & 30${}^\circ\pm 1$\\
            #        \Sextile & sextile & 60${}^\circ\pm 5$\\
            #        \Square & square & 90${}^\circ\pm 7$\\
            #        \Trine & trine & 120${}^\circ\pm 7$\\
            #        \Inconjunct & inconjunct & 150${}^\circ\pm 5$\\
            #        \Opposition & opposition & 180${}^\circ\pm 7$
          #        \end{tabular}'''
          t5 = r'''\begin{tabular}[t]{lll|}
          \Conjunction & conjunction & 0${}^\circ\pm 7$\\
          \Semisextile & semisextile & Omitted\\
          \Sextile & sextile & 60${}^\circ\pm 5$\\
          \Square & square & 90${}^\circ\pm 7$\\
          \Trine & trine & 120${}^\circ\pm 7$\\
          \Inconjunct & inconjunct & Omitted\\
          \Opposition & opposition & 180${}^\circ\pm 7$
          \end{tabular}'''
        else:
          t4 = r'''\begin{tabular}[t]{|ll|}
          \color{red}{\Aries} & \color{red}{Aries} \\
          \color{black}{\Taurus} & \color{black}{Taurus} \\
          \contour{black}{\textcolor{yellow}{\Gemini}} &
          \contour{black}{\textcolor{yellow}{Gemini}} \\
          \color{blue}{\Cancer} & \color{blue}{Cancer} \\
          \color{red}{\Leo} & \color{red}{Leo} \\
          \color{black}{\Virgo} & \color{black}{Virgo} \\
          \end{tabular}'''
          t5 = r'''\begin{tabular}[t]{|ll|}
          \contour{black}{\textcolor{yellow}{\Libra}} &
          \contour{black}{\textcolor{yellow}{Libra}} \\
          \color{blue}{\Scorpio} & \color{blue}{Scorpio} \\
          \color{red}{\Sagittarius} & \color{red}{Sagittarius} \\
          \color{black}{\Capricorn} & \color{black}{Capricorn} \\
          \contour{black}{\textcolor{yellow}{\Aquarius}} &
          \contour{black}{\textcolor{yellow}{Aquarius}} \\
          \color{blue}{\Pisces} & \color{blue}{Pisces} \\
          \end{tabular}'''
        t6 = r'''\begin{tabular}[t]{l}
        On a particular day, the \\
        moon may set in the morning \\
        before it rises in the evening. \\\hline
        Prepared with Astrum %s \\
        Uses ephem %s \\
        Uses data from http://geonames.org
        \end{tabular}''' % (astrum.astrum.__version__, ephem.__version__)
        t7 = r'''\begin{tabular}[t]{l}
        Prepared with Astrum %s \\
        Uses ephem %s \\
        Uses data from http://geonames.org
        \end{tabular}''' % (astrum.astrum.__version__, ephem.__version__)
        if self.extended:
            doc.append(NoEscape(
              '\n\n\\hrule\n\n{\\tiny\\vspace*{1ex}%s%s%s%s%s%s%s%s}' %
              (t0, t1, t2, t3, t3b,t4, t5, t6)))
        else:
          if table_count == 1 or table_count == 3:
            doc.append(NoEscape(
              '\n\n\\hrule\n\n{\\tiny\\vspace*{1ex}%s%s%s}' %
              (t0, t1, t2)))
          else:
            doc.append(NoEscape('\n\n\\hrule\n\n{\\tiny\\vspace*{1ex}%s%s%s}' %
                                (t3, t4, t7)))
        doc.append(NoEscape(r'\newpage'))
        count = 0
        cellcolor = True
        if self.date.year == self.year:
            page += 1
            table = self.create_table(doc, heading, page)
      if self.date.year != self.year:
        break
      count += 1
#      if count > 10:
#        break
#    doc.append(table)
    doc.generate_tex()
