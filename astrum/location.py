#!/usr/bin/env python3
# location.py -*-python-*-
# Copyright 2017 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

import ephem
import math
import pickle

from astrum.log import DEBUG, INFO, ERROR, FATAL
from astrum.util import OpenFile

database_read = False
datebase = None

class Location:
  def __init__(self, location = None, latitude = None, longitude = None,
               geonames = 'geonames.dat'):
    self.location = location
    # FIXME: Accept D:M:S format, too
    self.latitude = float(latitude) if latitude else None
    self.longitude = float(longitude) if longitude else None
    self.elevation = None
    self.timezone = None
    self.geonames = geonames
    self.read_database()

    if not self.location:
      if not self.latitude or not self.longitude:
        FATAL('No location provided: longitude and latitude are required')
      self.location = '%s %s' % (ephem.degrees(math.radians(self.longitude)),
                                 ephem.degrees(math.radians(self.latitude)))
    else:
      self.latitude, self.longitude = self.lookup_location(self.location)

  def __repr__(self):
    return self.location

  def __str__(self):
    return self.location

  def lat_lon(self):
    return (self.latitude, self.longitude)

  def read_database(self):
    global database_read
    global database
    if not database_read:
      INFO('Reading location data from %s' % self.geonames)
      with OpenFile(self.geonames) as fp:
        database = pickle.load(fp)
      INFO('%d locations loaded' % len(database))
      database_read = True
    self.names = database

  def lookup_location(self, location):
    requested_name, requested_admin1, requested_country, *_ \
      = location.split(', ') + [None] * 3
    match = self.names.get(requested_name.lower(), None)
    if match == None:
      FATAL('Location "%s" is not in the database' % location)

    candidates = []
    for name, admin1, admin2, country, timezone, \
        latitude, longitude, elevation in match:
      if (not requested_admin1 \
          or requested_admin1.lower() == admin1.lower()) \
          and (not requested_country
               or requested_country.lower() == country.lower()):
        candidates.append((name, admin1, admin2, country, timezone,
                           latitude, longitude, elevation))
    for name, admin1, admin2, country, timezone, \
        latitude, longitude, elevation in candidates:
      INFO('Found: %s, %s, %s (%s, Latitude=%s, Longitude=%s, %s)'
           % (name, admin1, country, admin2, latitude, longitude, timezone))
      self.location = '%s, %s, %s, (%s)' % (name, admin1, country, admin2)
      self.latitude = float(latitude)
      self.longitude = float(longitude)
      self.elevation = float(elevation)
      self.timezone = timezone

    if len(candidates) > 1:
      FATAL('Please make --location more specific')

    return self.latitude, self.longitude
