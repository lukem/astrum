#!/usr/bin/env python3
# main.py -*-python-*-
# Copyright 2017 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

import argparse
import datetime
import dateparser
import time

from astrum.log import pdlog_set_level, DEBUG, INFO, ERROR, FATAL
from astrum.astrum import Astrum
from astrum.year_summary import YearSummary

DEFAULT_LATITUDE = 36 + 5/60.
DEFAULT_LONGITUDE = -(79 + 6/60.)
DEFAULT_CITY = 'Hillsborough, NC'

def main():
  INFO('Astrum is open source software under the MIT License')
  INFO('City location data from http://geonames.org (CC-BY 3.0)')

  local_time = time.ctime()
  parser = argparse.ArgumentParser(
    description = 'Astrum Astrological Calculator',
    formatter_class = argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('-t', '--time', help = 'Local Time',
                      default = local_time)
  parser.add_argument('-l', '--location', help = 'City name',
                      default = DEFAULT_CITY)
  parser.add_argument('--latitude', help = 'Latitude',
                      default = '%0.2f' % DEFAULT_LATITUDE)
  parser.add_argument('--longitude', help = 'Longitude',
                      default= '%0.2f' % DEFAULT_LONGITUDE)
  parser.add_argument('--year', help = 'Output a year of planetary positions')
  parser.add_argument('--extended', action = 'store_true',
                      help = '4-page year summary (vs. 2 page)')
  parser.add_argument('-d', '--debug', action = 'store_true', default = False,
                      help = 'Very verbose logging for debugging')
  args = parser.parse_args()

  if args.debug:
    pdlog_set_level('DEBUG')

  if args.year:
    ys = YearSummary(int(args.year), args.location,
                     args.latitude, args.longitude, extended = args.extended)
    return 0


  ast = Astrum(args.time, args.location, args.latitude, args.longitude)
  return 0
