#!/usr/bin/env python3
# converter.py -*-python-*-
# Copyright 2017 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

import pickle
import time

from astrum.log import DEBUG, INFO, ERROR, FATAL
from astrum.util import OpenFile

class CompactDict(object):
  def __init__(self):
    self.to_dict = dict()
    self.from_dict = dict()
    self.idx = 0

  def lookup(self, value):
    if value in self.to_dict:
      return self.to_dict[value]
    self.to_dict[value] = self.idx
    self.from_dict[self.idx] = value
    self.idx += 1
    return self.idx - 1

  def get_dict(self):
    return self.from_dict

class Converter(object):
  def __init__(self, country_file, code_file, output_file):
    self.country_file = country_file
    self.code_file = code_file
    self.output_file = output_file
    self.codes = dict()
    self.names = dict()
    self.cdict = CompactDict()

  def lookup_code(self, country, admin1, admin2):
    code = country + '.' + admin1 + '.' + admin2
    return self.codes.get(code, '???')

  def write_data(self):
    INFO('Writing %s' % self.output_file)
    for t in self.names:
      INFO(t)
      break
    with open(self.output_file, 'wb') as fp:
      pickle.dump(self.names, fp)
    INFO('%d location names writtern to %s' % (len(self.names),
                                               self.output_file))

  def load_data(self):
    INFO('Reading %s' % self.code_file)
    with OpenFile(self.code_file, textmode = True) as fp:
      for line in fp:
        code, name, asciiname, geonameid = line.split('\t')
        if code[:3] != 'US.':
          continue
        self.codes[code] = name
    INFO('%d codes read from %s' % (len(self.codes), self.code_file))

    INFO('Reading %s' % self.country_file)
    features = 0
    report_time = time.time()
    with OpenFile(self.country_file, textmode = True) as fp:
      for line in fp:
        fields = line.split('\t')
        # Don't use a tuple here, since it will be easier to update if the
        # format changes using this inefficient verbosity.
        geonameid = fields[0]
        name = fields[1] # utf8
        asciiname = fields[2] # ascii
        alternatenames = fields[3]
        latitude = fields[4] # WSG84
        longitude = fields[5] # WSG84
        feature_class = fields[6] # http://www.geonames.org/export/codes.html
        feature_code = fields[7] # http://www.geonames.org/export/codes.html
        country = fields[8] # ISO-3166
        cc2 = fields[9]
        admin1 = fields[10] # FIPS
        admin2 = fields[11]
        admin3 = fields[12]
        admin4 = fields[13]
        population = fields[14]
        elevation = fields[15] # meters
        dem = fields[16] # srtm3 or gtopo30
        timezone = fields[17]
        modification_date = fields[18]

        features += 1
        if (time.time() - report_time > 2.0):
          if len(self.names):
            INFO('Reading progress: %d features, %d used locations (e.g., %s)'
                 % (features, len(self.names), entry))
          else:
            INFO('Reading progress: %d features, %d used locations'
                 % (features, len(self.names)))
          report_time = time.time()


        if feature_class != 'P' or country != 'US':
          continue

        entry = (name,
                 admin1,
                 #self.cdict.lookup(self.lookup_code(country, admin1, admin2)),
                 self.lookup_code(country, admin1, admin2),
                 country,
                 timezone, latitude, longitude, elevation)
        #self.cdict.lookup(timezone), latitude, longitude, elevation)

        previous = self.names.get(asciiname.lower(), [])
        previous.append(entry)
        self.names[asciiname.lower()] = previous

    INFO('%d location names used from %s' % (len(self.names),
                                             self.country_file))
