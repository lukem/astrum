#!/usr/bin/env python3
# __init__.py -*-python-*-
# Copyright 2016 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

'''
The Astrum Module
'''

from . import astrum
from .log import pdlog_init

setattr(astrum, '__version__', '2022.01.12')
pdlog_init()
