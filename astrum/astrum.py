#!/usr/bin/env python3
# astrum.py -*-python-*-
# Copyright 2017 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

# Standard imports
import math
import sys

# Imports that might have to be installed
try:
    import ephem
except ImportError as e:
    print('''\
# Cannot import ephem: {}
# Consider: apt-get install python3-ephem'''.format(e))
    raise SystemExit

from astrum.location import Location
from astrum.log import INFO, FATAL
from astrum.time import Time
from astrum.util import Util

class Astrum:
  TAU = 2.0 * ephem.pi

  SIGNS = [ 'Aries',
            'Taurus',
            'Gemini',
            'Cancer',
            'Leo',
            'Virgo',
            'Libra',
            'Scorpio',
            'Sagittarius',
            'Capricorn',
            'Aquarius',
            'Pisces' ]

  SIGN_COLORS = {'Aries' : 'red',
                 'Taurus' : 'black',
                 'Gemini' : 'yellow',
                 'Cancer' : 'blue',
                 'Leo' : 'red',
                 'Virgo' : 'black',
                 'Libra' : 'yellow',
                 'Scorpio' : 'blue',
                 'Sagittarius' : 'red',
                 'Capricorn' : 'black',
                 'Aquarius' : 'yellow',
                 'Pisces' : 'blue'
  }

  PLANETS = [ ( 'Sun', 'diurnal' ),
              ( 'Moon', 'nocturnal' ),
              ( 'Mercury', 'variable' ),
              ( 'Venus', 'nocturnal' ),
              ( 'Mars', 'nocturnal' ),
              ( 'Jupiter', 'diurnal' ),
              ( 'Saturn', 'diurnal' ),
              ( 'Uranus', None ),
              ( 'Neptune', None),
              ( 'Pluto', None),
  ]

  CHALDEAN = [ 'Saturn',
               'Jupiter',
               'Mars',
               'Sun',
               'Venus',
               'Mercury',
               'Moon'
  ]

  DAYS = [ 'Sunday',
           'Monday',
           'Tuesday',
           'Wednesday',
           'Thursday',
           'Friday',
           'Saturday'
  ]

  THELEMIC_DAYS = [ 'dies Solis',
                    'dies Lunae',
                    'dies Martis',
                    'dies Mercurii',
                    'dies Jovis',
                    'dies Veneris',
                    'dies Saturnii'
  ]


  PHASES = [ 'Waxing Crescent',
             'Waxing Gibbous',
             'Waning Gibbous',
             'Waning Crescent'
  ]

  ASPECTS = [ ('Square', 90, 7),
              ('Inconjunct', 150, 5),
              ('Opposition', 180, 7),
              ('Trine', 120, 7),
              ('Sextile', 60, 5),
              ('Semisextile', 30, 1),
              ('Conjunction', 0, 7)
  ]

  SHORT_ASPECTS = {'Square':      'square',
                   'Inconjunct':  'inconj.',
                   'Opposition':  'oppos.',
                   'Trine':       'trine',
                   'Sextile':     'sextile',
                   'Semisextile': 'semisex.',
                   'Conjunction': 'conjunc.'
                   }

  DIGNITY_ABBREVIATIONS = ( 'r', 'd', 'e', 'f')

  # order is : Rulership, Detriment (opposite Rulership), Exaltation, and Fall
  # (opposite Exaltation)
  DIGNITIES = { 'Sun' : (['Leo'], ['Aquarius'], ['Aries'], [' Libra']),
                'Moon' : (['Cancer'], ['Capricorn'], ['Taurus'], ['Scorpio']),
                'Mercury' : (['Gemini', 'Virgo'], ['Sagittarius', 'Pisces'],
                             ['Virgo'], ['Pisces']),
                'Venus' : (['Taurus', 'Libra'], ['Scorpio', 'Aries'],
                           ['Pisces'], ['Virgo']),
                'Mars' : (['Aries', 'Scorpio'], ['Libra', 'Taurus'],
                          ['Capricorn'], ['Cancer']),
                'Jupiter' : (['Sagittarius', 'Pisces'], ['Gemini', 'Virgo'],
                             ['Cancer'], ['Capricorn']),
                'Saturn' : (['Capricorn', 'Aquarius'], ['Cancer', 'Leo'],
                            ['Libra'], ['Taurus']),
                'Uranus' : (['Aquarius'], ['Leo'], ['Scorpio'],
                            ['Taurus']),
                'Neptune' : (['Pisces'], ['Virgo'], ['Leo'], ['Aquarius']),
                'Pluto' : (['Scorpio'], ['Taurus'], ['Aries'], ['Libra']),
  }


  ROMAN = [
    (1000, "M"),
    ( 900, "CM"),
    ( 500, "D"),
    ( 400, "CD"),
    ( 100, "C"),
    (  90, "XC"),
    (  50, "L"),
    (  40, "XL"),
    (  10, "X"),
    (   9, "IX"),
    (   5, "V"),
    (   4, "IV"),
    (   1, "I"),
  ]

  def __init__(self, local_time, location,
               latitude_degrees, longitude_degrees):
    self.location = Location(location,
                             latitude_degrees,
                             longitude_degrees)
    self.latitude_radians = math.radians(self.location.latitude)
    self.longitude_radians = math.radians(self.location.longitude)
    INFO('Default time zone is %s' % self.location.timezone)
    self.time = Time(local_time, default_timezone = self.location.timezone)
    INFO('Input data: %s (%s, %s) at %s (%s)'
         % (Util.lat_lon_to_string(*self.location.lat_lon()),
            ephem.degrees(math.radians(self.location.latitude)),
            ephem.degrees(math.radians(self.location.longitude)),
            self.time.local_time(), self.time.utc_time()))
    self.compute()
    self.write_diary_entry()

  def compute(self):
    self.ascendant_degrees = None
    self.ascendant_sign = None
    self.ascendant_offset = None

    self.midheaven_degrees = None
    self.midheaven_sign = None
    self.midheaven_offset = None

    self.sun_declination_radians = None
    self.sun_longitude_radians = None

    self.moon_rise = None
    self.moon_set = None
    self.moon_quarter = None
    self.moon_phase = None
    self.moon_fractional_age = None

    self.aspects = None

    self.compute_observer()
    self.compute_ascendant()
    self.compute_midheaven()
    self.compute_planets()
    self.compute_moon()
    self.compute_sun()
    self.compute_aspects()
    INFO('*********************************************')
    self.compute_thelemic_date()
    INFO('*********************************************')

  def compute_observer(self):
    self.observer = ephem.Observer()
    self.observer.lon = self.longitude_radians
    self.observer.lat = self.latitude_radians
    if self.location.elevation:
      self.observer.elevation = self.location.elevation
    self.observer.date = self.time.ephem_time()
    self.observer.compute_pressure()

    self.universal_time = self.observer.date
    self.sidereal_time = self.observer.sidereal_time()
    self.sidereal_radians = float(self.sidereal_time)
    self.sidereal_degrees = math.degrees(self.sidereal_radians)
    self.local_time = ephem.localtime(self.universal_time)

    INFO('Assuming elevation %dm, temperature %d\u00b0C, pressure %dmBar'
         % (self.observer.elevation,
            self.observer.temp,
            self.observer.pressure))
    INFO('Universal Time: %s' % self.universal_time)
    INFO('Julian Date: %.6f' % ephem.julian_date(self.observer))
    INFO('Sidereal Time: %s = %0.4f\u00b0 = %0.4frad'
         % (self.sidereal_time, self.sidereal_degrees, self.sidereal_radians))
    INFO('Local Time: %s' % self.local_time)

  def compute_ascendant(self):
    '''
    Compute the ascendant angle and the corresponding rising sign.
    See http://en.wikipedia.org/wiki/Ascendant
    '''
    if self.observer.epoch == ephem.J2000:
      obliquity_radians = math.radians(23.4392911)
    else:
      obliquity_radians = math.radians(23.4457889)
    ascendant_radians = math.atan2(
      -math.cos(self.sidereal_radians),
      (math.sin(self.sidereal_radians) * math.cos(obliquity_radians)
       + math.tan(self.latitude_radians) * math.sin(obliquity_radians)))

    if ascendant_radians < math.radians(180):
      ascendant_radians += math.radians(180)
    else:
      ascendant_radians -= math.radians(180)

    self.ascendant_degrees = math.degrees(ascendant_radians)
    self.ascendant_sign = int(self.ascendant_degrees / 30.)
    self.ascendant_offset = self.ascendant_degrees - 30. * self.ascendant_sign

    INFO('Ascendant: %s' % ephem.degrees(math.radians(self.ascendant_degrees)))
    INFO('AC: %s %s' % (Astrum.SIGNS[self.ascendant_sign],
                        ephem.degrees(math.radians(self.ascendant_offset))))

  def compute_midheaven(self):
    '''
    Compute the midheaven angle and the corresponding rising sign.
    See https://en.wikipedia.org/wiki/Midheaven
    '''
    if self.observer.epoch == ephem.J2000:
      obliquity_radians = math.radians(23.4392911)
    else:
      obliquity_radians = math.radians(23.4457889)
    midheaven_radians = math.atan2(math.tan(self.sidereal_radians),
                                   math.cos(obliquity_radians))
    if midheaven_radians < math.radians(180):
      midheaven_radians += math.radians(180)
    else:
      midheaven_radians -= math.radians(180)

    self.midheaven_degrees = math.degrees(midheaven_radians)
    self.midheaven_sign = int(self.midheaven_degrees / 30.)
    self.midheaven_offset = self.midheaven_degrees - 30. * self.midheaven_sign

    INFO('Midheaven: %s' % ephem.degrees(math.radians(self.midheaven_degrees)))
    INFO('MC: %s %s' % (Astrum.SIGNS[self.midheaven_sign],
                        ephem.degrees(math.radians(self.midheaven_offset))))

  def compute_moon(self):
    angle = (self.moon_longitude_radians
             - self.sun_longitude_radians) % Astrum.TAU
    self.moon_quarter = int(angle * 4.0 // Astrum.TAU)

    try:
      INFO('Moon Rise: %s' % ephem.localtime(self.moon_rise))
    except:
      INFO('Moon Rise: %s' % self.moon_rise)
    try:
      INFO('Moon Set: %s' % ephem.localtime(self.moon_set))
    except:
      INFO('Moon Set: %s' % self.moon_set)

    next_new_moon = ephem.next_new_moon(self.observer.date)
    prev_new_moon = ephem.previous_new_moon(self.observer.date)
    self.moon_fractional_age = ((self.observer.date - prev_new_moon) /
                                (next_new_moon - prev_new_moon))

    INFO('Moon is %s, %d%% of Full, %.1f days old' %
         (Astrum.PHASES[self.moon_quarter],
          self.moon_phase,
          self.moon_fractional_age * (next_new_moon - prev_new_moon)))
    INFO('Next full moon: %s'
         % ephem.localtime(ephem.next_full_moon(self.observer.date)))
    INFO('Next new moon: %s' % next_new_moon)

  def compute_sun(self):
    pass

  def compute_planets(self):
    self.planets = dict()
    for name, sect in Astrum.PLANETS:
      planet, latitude_radians, longitude_radians, declination_radians, \
        phase = self.get_position(name)
      velocity = self.get_velocity(name)
      retrograde = velocity < 0.

      midnight_observer = ephem.Observer()
      midnight_observer.lon = self.observer.lon
      midnight_observer.lat = self.observer.lat
      midnight_observer.elevation = self.observer.elevation
      midnight_observer.date = self.time.ephem_midnight()
      midnight_observer.compute_pressure()

      if name == 'Sun':
        self.sun_declination_radians = declination_radians
        self.sun_longitude_radians = longitude_radians
        self.sun_rise = ephem.localtime(midnight_observer.next_rising(planet))
        self.sun_set = ephem.localtime(midnight_observer.next_setting(planet))
        INFO('Sun Rise: %s' % self.sun_rise)
        INFO('Sun Set: %s' % self.sun_set)

      if name == 'Moon':
        self.moon_longitude_radians = longitude_radians
        self.moon_phase = phase
        self.moon_rise = ephem.localtime(midnight_observer.next_rising(planet))
        self.moon_set = ephem.localtime(midnight_observer.next_setting(planet))

      longitude_degrees = math.degrees(longitude_radians)
      latitude_degrees = math.degrees(latitude_radians)
      declination_degrees = math.degrees(declination_radians)
      sign = int(longitude_degrees / 30.)
      offset = longitude_degrees - 30. * sign

      rejoicing = self.is_rejoicing(sect, longitude_radians,
                                    declination_radians)

      if name == 'Sun':
        INFO('%-7.7s    %-12.12s %11.11s %11.11s %11.11s %11.11s' % (
          'Planet', 'Sign', 'Longitude', 'Velocity', 'Latitude',
          'Declination'))

      INFO('%-7.7s %s%s %-12.12s %11.11s %11.11s %11.11s %11.11s' % (
        name,
        '!' if rejoicing else ' ',
        'R' if retrograde else ' ',
        Astrum.SIGNS[sign],
        ephem.degrees(offset * ephem.degree),
        ephem.degrees(velocity),
        ephem.degrees(latitude_radians),
        ephem.degrees(declination_radians)))

      self.planets[name] = (rejoicing, retrograde, longitude_degrees,
                            sign, offset, velocity,
                            latitude_degrees, declination_degrees)
      if name == 'Moon' and retrograde:
        FATAL('Moon cannot be retrograde')

  def get_longitude(self, name, date):
    planet = ephem.__dict__[name](date)
    ecliptic = ephem.Ecliptic(ephem.Equatorial(planet.ra, planet.dec))
    return ecliptic.lon

  def get_position(self, name):
    planet = ephem.__dict__[name](self.observer)
    right_ascension = planet.g_ra
    declination_radians = planet.g_dec
    ecliptic = ephem.Ecliptic(ephem.Equatorial(right_ascension,
                                               declination_radians))
    latitude_radians = ecliptic.lat
    longitude_radians = ecliptic.lon
    return [planet, latitude_radians, longitude_radians, declination_radians,
            planet.phase]

  def get_velocity(self, name):
    longitude0 = self.get_longitude(name, self.observer.date - 0.5)
    longitude1 = self.get_longitude(name, self.observer.date + 0.5)
    difference = longitude1 - longitude0
    difference -= math.radians(360) if difference > math.radians(180) else 0
    difference += math.radians(360) if difference < math.radians(-180) else 0
    return difference

  def is_rejoicing(self, sect, longitude_radians, declination_radians):
    if sect is None:
      return False
    if sect == 'variable':
      # Mercury is diurnal if it rises before the Sun (oriental), and is
      # nocturnal if it rises after the Sun (occidental)
      if longitude_radians < self.sun_longitude_radians:
        sect = 'diurnal'
      else:
        sect = 'nocturnal'

    if sect == 'diurnal':
      # Planet is in the same hemisphere as the Sun
      rejoicing = self.hemisphere(declination_radians) \
                  == self.hemisphere(self.sun_declination_radians)
    elif sect == 'nocturnal':
      # Planet is in the hemisphere opposite the Sun
      rejoicing = self.hemisphere(declination_radians) \
                  != self.hemisphere(self.sun_declination_radians)

    return rejoicing

  def hemisphere(self, angle):
    if angle < 0:
      return -1
    return 1

  def compute_aspects(self):
    self.aspects = []
    for (planet, _) in Astrum.PLANETS:
      (_, _, longitude_degrees, *_) = self.planets[planet]
      start = False
      for (aspected, _) in Astrum.PLANETS:
        if aspected == planet:
          start = True
          continue
        if not start:
          continue
        (_, _, aspected_longitude_degrees, *_) = self.planets[aspected]
        difference = abs(float(longitude_degrees)
                         - float(aspected_longitude_degrees))
        for aspect, angle, orb in Astrum.ASPECTS:
          if difference >= angle - orb and difference <= angle + orb:
            self.aspects.append((angle, planet, aspect,
                                 angle - difference, aspected))
            INFO('%s %s %.1f %s' % (planet, aspect, angle - difference,
                                    aspected))

  def to_roman(self, number):
    result = ""
    for (arabic, roman) in Astrum.ROMAN:
      (factor, number) = divmod(number, arabic)
      result += roman * int(factor)
    return result

  def compute_thelemic_date(self, fp=None, prefix=None):
    _, _, _, sun_sign, sun_degrees, *_ = self.planets['Sun']
    _, _, _, moon_sign, moon_degrees, *_ = self.planets['Moon']
    anno = self.local_time.year - 1904
    if self.local_time.month in [1, 2]:
      anno -= 1
    if self.local_time.month == 3 and sun_sign == 11:
      anno -= 1
    docosade = anno / 22
    year = anno % 22
    weekday = (self.local_time.weekday() + 1) % 7
    if fp and not prefix:
      fp.write(
        '\\Sol{} %d \\%s, \\Luna{} %d \\%s, \\textit{%s}, %s%s\n' %
        (int(sun_degrees),
         self.SIGNS[sun_sign],
         int(moon_degrees),
         self.SIGNS[moon_sign],
         Astrum.THELEMIC_DAYS[weekday],
         self.to_roman(docosade),
         self.to_roman(year).lower()))
    elif fp and prefix:
      fp.write(
        '%sSun %d %s, Moon %d %s, %s, %s%s\n' %
        (prefix,
         int(sun_degrees),
         self.SIGNS[sun_sign],
         int(moon_degrees),
         self.SIGNS[moon_sign],
         Astrum.THELEMIC_DAYS[weekday],
         self.to_roman(docosade),
         self.to_roman(year).lower()))
    else:
      INFO('Sun %d %s, Moon %d %s, %s, %s%s' % (int(sun_degrees),
                                                self.SIGNS[sun_sign],
                                                int(moon_degrees),
                                                self.SIGNS[moon_sign],
                                                Astrum.THELEMIC_DAYS[weekday],
                                                self.to_roman(docosade),
                                                self.to_roman(year).lower()))

  def write_diary_entry_tex(self):
    with open('astrum.tex', 'w') as fp:
      fp.write(r'''
\documentclass[12pt]{article}
\usepackage{../magickfonts}
\usepackage{array}
\usepackage{color}
\parskip 4ex
\parindent 0pt
\def\Square{\SSquare}
\def\Inconjunct{\NQuincunx}
\def\Opposition{\SOpposition}
\def\Trine{\STrine}
\def\Sextile{\SSextile}
\def\Semisextile{\SSemisextile}
\def\Conjunction{\SConjunction}
\newenvironment{diary}[2]{\rule{\textwidth}{1pt}}{}
\newcolumntype{C}[1]{%
      >{\vbox to 1.5ex\bgroup\vfill\centering}%
      m{#1}%
      <{\vskip-1.1\baselineskip\vfill\egroup}}
\newcolumntype{R}[1]{%
      >{\vbox to 1.5ex\bgroup\vfill\raggedleft}%
      m{#1}%
      <{\vskip-1.1\baselineskip\vfill\egroup}}
\begin{document}
      ''')
      fp.write(self.local_time.strftime('\\begin{diary}{%Y}{%j}\n\n'))
      fp.write('\\begin{minipage}[t]{.6\\textwidth}\\large\\bf\n')
      fp.write(self.local_time.strftime('%A, %d %B %Y \\textsc{e.v.}\n\n'))
      self.compute_thelemic_date(fp)
      fp.write('\n\n\\normalsize\\Luna{} %s, %d\\%% of Full\\\\\n\n' %
               (Astrum.PHASES[self.moon_quarter], self.moon_phase))
      fp.write('\\end{minipage}\\hfill\\begin{minipage}[t]{.39\\textwidth}\,\n')
      fp.write('\\begin{tabular}{|C{.03\\textwidth}|C{.03\\textwidth}|C{.03\\textwidth}|C{.03\\textwidth}|C{.03\\textwidth}|C{.03\\textwidth}|C{.03\\textwidth}R{0.5\\textwidth}}\n')
      for left, _ in Astrum.PLANETS:
        remaining = 8
        for right, _ in Astrum.PLANETS:
          remaining -= 1
          if left == right:
            planet = right
            if planet == 'Sun':
              planet = 'Sol'
            if planet == 'Moon':
              planet = 'Luna'
            _, retrograde, _, sign, offset, *_ = self.planets[right]
            fp.write('\\multicolumn{%d}{l}{\\%s} {\color{%s}\\%s{}} %d \\%s\\\\' %
                     (remaining,
                      planet,
                      'red' if retrograde else 'black',
                      planet,
                      offset,
                      Astrum.SIGNS[sign]))
            if remaining > 1:
              fp.write('\\cline{1-%d}\n' % (8 - remaining))
            else:
              fp.write('\\cline{1-6}\n')
            break
          for _, p0, aspect, _, p1 in self.aspects:
            if (p0 == left and p1 == right) or (p1 == left and p0 == right):
              fp.write('\\%s &' % aspect)
              break
          else:
            fp.write(' &')
      fp.write('\\end{tabular}\n')
      fp.write('\\end{minipage}\n')
      fp.write('\n\n\\end{diary}\n')

      fp.write('''
\end{document}
      ''')

  def write_diary_entry(self):
    # Don't use with, since that will close sys.stdout
    fp = sys.stdout
    fp.write('======================================================================\n')
    fp.write(self.local_time.strftime('entry:  %Y %j\n'))
    fp.write(self.local_time.strftime('date:   %A, %d %B %Y\n'))
    self.compute_thelemic_date(fp, 'legis:  ')
    fp.write('moon:   %s, %d%% of Full\n' %
             (Astrum.PHASES[self.moon_quarter], self.moon_phase))

    for planet, _ in Astrum.PLANETS:
      _, retrograde, _, sign, offset, *_ = self.planets[planet]
      fp.write('planet: %-7.7s %2d %s%s\n' %
               (planet, offset, Astrum.SIGNS[sign],
                '' if not retrograde else ' (retro)'))
    for left, _ in Astrum.PLANETS:
      remaining = 8
      fp.write('aspect: ')
      for right, _ in Astrum.PLANETS:
        remaining -= 1
        if left == right:
          fp.write('%7.7s |\n' % right)
          break
        for _, p0, aspect, _, p1 in self.aspects:
          if (p0 == left and p1 == right) or (p1 == left and p0 == right):
            fp.write('%7.7s |' % Astrum.SHORT_ASPECTS[aspect])
            break
        else:
          fp.write('        |')
