#!/usr/bin/env python3
# util.py -*-python-*-
# Copyright 2017 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

# Standard imports
import os
import subprocess

# Imports that might have to be installed
try:
    import dateparser
except ImportError as e:
    print('''\
# Cannot import dateparser: {}
# Consider: apt-get install python3-dateparser'''.format(e))
    raise SystemExit

def monkeypatch_method(cls):
  '''
  From Guido van Rossum, Jan 15, 2008
  See https://mail.python.org/pipermail/python-dev/2008-January/076194.html

  To use, decorate new method with @monkeypatch_method(<someclass>)
  '''
  def decorator(func):
    setattr(cls, func.__name__, func)
    return func
  return decorator

class Util(object):
  @staticmethod
  def parse_date(time):
    return dateparser.parse(time).timestamp()

  @staticmethod
  def lat_lon_to_string(latitude, longitude):
    '''
    Return string of latitude and longitude in ISO-6709 compatible format.
    See https://en.wikipedia.org/wiki/ISO_6709
    '''
    if latitude >= 0:
      s = '%0.2f\u00b0N' % float(latitude)
    else :
      s = '%0.2f\u00b0S' % -float(latitude)

    if longitude >= 0:
      s += ' %0.2f\u00b0E' % float(longitude)
    else:
      s += ' %0.2f\u00b0W' % -float(longitude)
    return s


class OpenFile(object):
  def __init__(self, filename, textmode = False):
    self.filename = filename
    self.textmode = textmode

  def __enter__(self):
    if os.path.exists(self.filename):
      filename = self.filename
    elif os.path.exists(self.filename + '.gz'):
      filename = self.filename + '.gz'
    else:
      ERROR('%s does not exist' % self.filename)

    try:
      if filename.endswith('.gz') or filename.endswith('.zip'):
        self.pipe = subprocess.Popen(['zcat', filename],
                                     universal_newlines = self.textmode,
                                     stdout = subprocess.PIPE)
      else:
        self.pipe = subprocess.Popen(['cat', filename],
                                     universal_newlines = self.textmode,
                                     stdout = subprocess.PIPE)
    except IOError as e:
      FATAL('Cannot open {}: {}'.format(filename, e.stderror))
    return self.pipe.stdout

  def __exit__(self, type, value, traceback):
    if self.pipe:
      self.pipe.kill()
      self.pipe.wait()
