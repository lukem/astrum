#!/usr/bin/env python3
# log.py -*-python-*-
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>
#
# Usage:
#   from <> import DEBUG, INFO, ERROR, FATAL
# or, if setting the level:
#   from <> import pdlog_set_level, DEBUG, INFO, ERROR, FATAL

import logging
import sys
import time

pdlog_initial_level_set = False

def pdlog_format_message(record):
  try:
    msg = '%s' % (record.msg % record.args)
  except:
    msg = record.msg
  return msg

def pdlog_set_level(newlevel):
  old_level = logger.getEffectiveLevel()
  if newlevel == 'DEBUG':
    logger.setLevel(10)
  elif newlevel == 'INFO':
    logger.setLevel(20)
  elif newlevel == 'ERROR':
    logger.setLevel(40)
  else:
    logger.setLevel(newlevel)
  new_level = logger.getEffectiveLevel()

  global pdlog_initial_level_set
  if pdlog_initial_level_set:
    logger.info('Logging changed from level {} ({}) to level {} ({})'.format(
      old_level,
      logging.getLevelName(old_level),
      new_level,
      logging.getLevelName(new_level)))
  pdlog_initial_level_set = True

def pdlog_init():
  pdlog_set_level('INFO')

class PDLogFormatter(logging.Formatter):
  def __init__(self):
    logging.Formatter.__init__(self)
    self.timestamps = True
    self.lineno = False

  def format(self, record):
    level = record.levelname[0]
    if self.timestamps:
      if self.lineno:
        date = time.localtime(record.created)
        date_msec = (record.created - int(record.created)) * 1000
        message = '%c%04d%02d%02d %02d:%02d:%02d.%03d %s:%d: %s' % (
          level,
          date.tm_year, date.tm_mon, date.tm_mday,
          date.tm_hour, date.tm_min, date.tm_sec, date_msec,
          record.filename, record.lineno,
          pdlog_format_message(record))
      else:
        date = time.localtime(record.created)
        date_msec = (record.created - int(record.created)) * 1000
        message = '%c%04d%02d%02d %02d:%02d:%02d.%03d %s' % (
          level,
          date.tm_year, date.tm_mon, date.tm_mday,
          date.tm_hour, date.tm_min, date.tm_sec, date_msec,
          pdlog_format_message(record))
    else:
      if self.lineno:
        message = '%c %s:%d: %s' % (
          level,
          record.filename, record.lineno,
          pdlog_format_message(record))
      else:
        message = '%c %s' % (
          level,
          pdlog_format_message(record))

    record.getMessage = lambda: message
    return logging.Formatter.format(self, record)

logger = logging.getLogger()
logging.addLevelName(50, 'FATAL')

handler = logging.StreamHandler()
handler.setFormatter(PDLogFormatter())

logger.addHandler(handler)

def PDLogFatal(message, *args, **kwargs):
  logging.fatal(message, args, kwargs)
  sys.exit(1)

DEBUG = logging.debug
INFO = logging.info
ERROR = logging.error
FATAL = PDLogFatal
