#!/usr/bin/env python3
# time.py -*-python-*-
# Copyright 2017 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

import dateparser
import datetime
import pytz
import re

from astrum.log import DEBUG, INFO, ERROR, FATAL

class Time:
  def __init__(self, local_time, default_timezone = None):
    if default_timezone == None:
      default_timezone = 'US/Eastern'
    if type(local_time) == type(datetime.datetime(1900, 1, 1)):
      self.datetime = local_time
    else:
      self.datetime \
        = dateparser.parse(local_time,
                           settings = { 'TIMEZONE' : default_timezone,
                                        'RETURN_AS_TIMEZONE_AWARE' : True })
    if not self.datetime:
      local_time = re.sub(r'([0-9])[aA]([^mM]|$)', r'\1am ', local_time)
      local_time = re.sub(r'([0-9])[pP]([^mM]|$)', r'\1pm ', local_time)
      INFO('Trying to parse modified time %s' % local_time)
      self.datetime \
        = dateparser.parse(local_time,
                           settings = { 'TIMEZONE' : default_timezone,
                                        'RETURN_AS_TIMEZONE_AWARE' : True })
    if not self.datetime:
      FATAL('Cannot parse date: %s' % local_time)
    self.timezone = self.datetime.tzname()
    if self.timezone == None:
      self.datetime = pytz.timezone(default_timezone).localize(self.datetime)
      self.timezone = self.datetime.tzname()

  def __repr__(self):
    return self.local_time()

  def __str__(self):
    return self.local_time()

  def local_time(self):
    # Www Mmm dd hh:mm:ss ZZZ yyyy
    return self.datetime.strftime('%a %b %e %H:%M:%S %Z %Y')

  def utc_time(self):
    return self.datetime.astimezone(
      pytz.timezone('UTC')).strftime('%a %b %e %H:%M:%S %Z %Y')

  def unix_time(self):
    return time.mktime(self.datetime.timetuple())

  def ephem_time(self):
    return self.datetime.astimezone(
      pytz.timezone('UTC')).strftime('%Y/%m/%d %H:%M:%S')

  def ephem_midnight(self):
    d = self.datetime.replace(hour = 0, minute = 0, second = 0)
    d = d.astimezone(pytz.timezone('UTC'))
    return d.strftime('%Y/%m/%d %H:%M:%S')
