#!/usr/bin/env python3
# fetcher.py -*-python-*-
# Copyright 2016 by Luke Meyers (lukem1900@gmail.com)
# This program comes with ABSOLUTELY NO WARRANTY.

import lxml.etree
import os
import sys
import time
import traceback
import urllib.request

from astrum.log import DEBUG, INFO, ERROR, FATAL
from astrum.util import Util

class Fetcher(object):
  def __init__(self, baseurl, files, target_directory, dryrun = False):
    self.baseurl = baseurl
    self.files = files
    self.target_directory = target_directory
    self.local_timestamps = dict()
    self.remote_timestamps = dict()
    self.dryrun = dryrun

  def fetch(self):
    self.get_local_timestamps()
    self.get_remote_timestamps()
    self.select_remote_files()

  def get_local_timestamps(self):
    for file in self.files:
      path = os.path.join(self.target_directory, file)
      if not os.path.exists(path):
        self.local_timestamps[file] = None
        INFO('%s does not exist' % path)
        continue
      stat = os.stat(path)
      self.local_timestamps[file] = stat.st_mtime
      INFO('%s has local timestamp %s'
           % (path, time.ctime(self.local_timestamps[file])))

  def get_remote_timestamps(self):
    try:
      response = urllib.request.urlopen(self.baseurl)
      result = response.read()
      response.close()
    except:
      type, value = sys.exc_info()[:2]
      exc = traceback.format_exception_only(type, value)
      ERROR('Cannot open %s: %s' % (self.baseurl, exc[0]))
      return

    parser = lxml.etree.HTMLParser()
    tree = lxml.etree.fromstring(result, parser)

    filename = None
    date = None
    for link in tree.getiterator():
      if link.tag == 'a' and link.text in self.files:
        timestamp = link.tail.split()[:2]
        timestamp = ' '.join(timestamp)
        timestamp = Util.parse_date(timestamp)
        INFO('%s has remote timestamp %s' % (link.text, time.ctime(timestamp)))
        self.remote_timestamps[link.text] = timestamp

  def select_remote_files(self):
    for file in self.files:
      path = os.path.join(self.target_directory, file)
      url = os.path.join(self.baseurl, file)
      if self.local_timestamps[file] \
         and self.local_timestamps[file] >= self.remote_timestamps[file]:
        INFO('%s is up to date' % file)
        continue
      self.copy_url_to_file(url, path)

  def copy_url_to_file(self, url, target):
    def chunk_read(response, total_size, fp, chunk_size=64*1024):
      start = time.time()
      bytes_read = 0
      previous = 0

      while True:
        chunk = response.read(chunk_size)
        if not chunk:
          break

        bytes_read += len(chunk)

        fp.write(chunk)

        elapsed = time.time() - start
        if elapsed >= previous + 2:
          previous = elapsed
          rate = bytes_read / 1024.0 / elapsed
          percent = 100 * bytes_read / float(total_size)
          eta = (float(total_size) - float(bytes_read)) / 1024.0 / rate
          eta_minutes = int(eta / 60.0)
          eta_seconds = eta - eta_minutes * 60.0
          eta = '%d:%02d' % (eta_minutes, eta_seconds)
          INFO('%s: %d bytes (%0.1fKB/s) %d%% ETA=%s' % (target,
                                                         bytes_read,
                                                         rate,
                                                         percent,
                                                         eta))
      elapsed = time.time() - start
      if elapsed:
        rate = total_size / 1024.0 / elapsed
        INFO('%s: %d bytes (%0.1fKB/s)' % (target, total_size, rate))
      else:
        INFO('%s: %d bytes' % (target, total_size))

    response = urllib.request.urlopen(url, cafile=None, capath=None)
    try:
        response = urllib.request.urlopen(url)
    except:
        type, value = sys.exc_info()[:2]
        exc = traceback.format_exception_only(type, value)
        ERROR('Cannot open %s: %s' % (url, exc[0]))
        return

    total_size = response.info().get('Content-Length').strip()
    total_size = int(total_size)
    if os.path.exists(target):
      dst_size = os.path.getsize(target)
      if int(dst_size) == total_size:
        INFO('Already have %s (%s bytes)' % (target, dst_size))
        return
      else:
        INFO('Already have %s (%s bytes), but new version has %s bytes'
             % (target, dst_size, total_size))

    if self.dryrun:
      INFO('Would have fetched %s bytes from %s to %s' % (total_size, url,
                                                          target))
      return

    INFO('Fetching %s bytes from %s to %s' % (total_size, url, target))
    with open(target, 'wb') as fp:
      chunk_read(response, total_size, fp)
